# go

Various small projects alongside learning **The Go Programming Language**

## ataka
attacks a URL with concurrent N requests and returns the bytes read and response times for each reply plus the total _ataka_ run-time.
